<div align="center">

# Nixwriter - Create Bootable Linux images


![](https://i.imgur.com/l8MJuCW.png)

![](https://i.imgur.com/sfaUQvJ.png)

![](https://i.imgur.com/3xM8v9c.png)
</div>

## Installation
### Automatic installation
#### Arch
You can get AUR package [here](https://aur.archlinux.org/packages/nixwriter/). (Outdated)
### Manual installation

You will need `make`, and additionally `flatpak-builder` (for flatpak builds) in your system.

* ### Flatpak (Recommended): 

    * `git clone` this repo and `cd` into it
    * `make flatpak-initiate`
    * `make flatpak-install`

* ### Legacy method:
    * ### Dependencies 
        * Cargo and the Rust compiler
        * GTK+3 devel libraries
        * Curl and Bash
    * `git clone` this repo and `cd` into it
    * Issue `make && sudo make install`