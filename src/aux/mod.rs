pub mod backend;

pub mod consts {
    pub const APP_ID: &'static str = "com.gitlab.adnan338.Nixwriter";
    pub const BS: usize = 1024 * 1024 * 2;
}
